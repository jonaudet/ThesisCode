---
title: "Thesis analysis and figures"
author: "Jonathan Audet"
date: "August 25, 2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
if(!require(checkpoint)){
  install.packages("checkpoint")
  library(checkpoint)
}
checkpoint("2016-02-15")
if(!require(dplyr)){
  install.packages("dplyr")
  library(dplyr)
}
if(!require(tidyr)){
  install.packages("tidyr")
  library(tidyr)
}
if(!require(ggplot2)){
  install.packages("ggplot2")
  library(ggplot2)
}
if(!require(rstan)){
  install.packages("rstan")
  library(rstan)
}
if(!require(survival)){
  install.packages("survival")
  library(survival)
}

rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
```


