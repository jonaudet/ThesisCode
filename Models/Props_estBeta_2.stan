data{
  int N;
  int P;
  int Counts[P, N];
  int J;
  int<lower = 1, upper = J> group[N];
  int Totals[N];
}
#transformed data{
#
#  for(i in 1:N)
#    Totals[i] = sum(Counts[, i]);
#}
parameters{
  vector[P] logit_avg[J];
  vector<lower = 0>[P] pop_size[J];
}
transformed parameters{
  vector<lower = 0, upper = 1>[P] avg[J];

  for(j in 1:J)
    for(p in 1:P)
      avg[j, p] = inv_logit(logit_avg[j, p]);
}
model{
  for(i in 1:J){
    logit_avg[i,] ~ normal(-10, 10);
    pop_size[i,] ~ student_t(3, 0, 30);
  }
  for(i in 1:N)
    for(p in 1:P)
      Counts[p, i] ~ beta_binomial(Totals[i],
        avg[group[i], p] * pop_size[group[i], p],
        (1 - avg[group[i], p]) * pop_size[group[i], p]);
}
generated quantities{
  vector[N] cd107as;

  for(i in 1:N) cd107as[i] = to_real(Counts[i, 1] ./ Totals[i];
}
