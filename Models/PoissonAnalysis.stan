data{
  int N; //# of points to process
  int P; //# of populations
  int J; //# of groups
  int<lower = 1, upper = P> pop_ID[N]; //Population index (1..P)
  int<lower = 1, upper = J> grp_ID[N]; //Group index
  vector<lower = 0>[N] parent_totals; //Total population from which we have positives
  int D21_Pos_count[N]; // # of Positives
  vector[N] D0_Pos_count;
}
transformed data{
  vector[N] D0;

  for(i in 1:N)
    D0[i] = D0_Pos_count[i] > 0 ? log(D0_Pos_count[i]):-13;
}
parameters{
  //vector[P] gamma0[J];
  vector[P] gamma1[J];
  real<lower = 0> overdisp;
}
model{
  vector[N] rates;

  for(j in 1:J)
    for(p in 1:P){
    //gamma0[j, p] ~ normal(0, 10);
    gamma1[j, p] ~ normal(0, 1);
  }
  overdisp ~ student_t(5, 0, 5);

  for(n in 1:N)
    rates[n] = D0[n] + gamma1[grp_ID[n], pop_ID[n]];

  D21_Pos_count ~ neg_binomial_2_log(rates + log(parent_totals), overdisp);
}
