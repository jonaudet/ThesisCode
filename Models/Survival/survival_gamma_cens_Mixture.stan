data{
  int<lower = 0> N_obs;
  vector[N_obs] time;

  int N_cens;
  vector<lower = 0>[N_cens] time_cens;

  int groups[N_obs + N_cens];
  
  int<lower = 1> N_PBS;
  vector[N_PBS] time_Ctrl;
}
parameters{
  vector[2] logit_props;
  ordered[2] means;
  vector[2] log_shapes;
  vector<lower = 0>[N_cens] d_cens;
}
transformed parameters{
  vector<lower = 0>[N_cens] time_cens_estimate;
  vector[N_obs + N_cens] time_all;
  vector[2] props;
 
  time_cens_estimate = time_cens + d_cens;

  time_all = append_row(time, time_cens_estimate);

    for(i in 1:2)
    props[i] = inv_logit(logit_props[i]);

}
model{
  vector[2] rates;
  vector[2] shapes;

  log_shapes ~ normal(2, 1);
  means ~ gamma(1.2, 0.03);
  d_cens ~ normal(0, 300);
  logit_props ~ normal(0, 2.5);

  shapes = exp(log_shapes);

  rates = shapes ./ means;

  time_Ctrl ~ gamma(shapes[1], rates[1]);
  
  for(i in 1:(N_obs + N_cens)){
    target += log_mix(props[groups[i]], gamma_lpdf(time_all[i] | shapes[1], rates[1]), gamma_lpdf(time_all[i] | shapes[2], rates[2]));
  }
}