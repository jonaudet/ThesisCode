data{
  int N; //# of points to process
  vector<lower = 0>[N] parent_totals; //Total population from which we have positives
  int Pos_count[N]; // # of Positives
  int<lower = 0, upper = 1> D21[N]; //Is the sample from Day 21 (== 1) or Day 0 (== 0)?
}
parameters{
  real intercepts;
  real difference;
  real<lower = 0> overdisp;
}
model{
  vector[N] rates;

  intercepts ~ normal(0, 10);
  difference ~ normal(0, 1);
  overdisp ~ student_t(5, 0, 5);

  for(n in 1:N)
    rates[n] = intercepts + D21[n] * difference;

  Pos_count ~ neg_binomial_2_log(rates + log(parent_totals), overdisp);
}
