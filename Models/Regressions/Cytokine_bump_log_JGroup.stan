data{
  int N;  //Number of Concentrations
  int N_indiv; //Number of inidividuals
  int N_groups;
  vector[N] y; //Concentration
  vector[N] x; //Times
  int<lower = 1, upper = N_indiv> indiv_ID[N]; //Individual to whom the observation belongs
  int<lower = 1, upper = N_groups> group_ID[N_indiv];
  int<lower = 0, upper = 1> fit;
  
  vector[N_indiv] starts;
}
parameters{
  real mu_log_max_conc;
  real<lower = 0> sigma_between_log_max_conc;
  vector<lower = 0>[N_groups] sigma_within_log_max_conc;
  vector[N_groups] raw_between_log_max_conc;
  vector[N_indiv] raw_within_log_max_conc;
  real<lower = 0> tau_log_max_conc;
  
  real mu_log_max_conc_day;
  real<lower = 0> sigma_between_log_max_conc_day;
  vector<lower = 0>[N_groups] sigma_within_log_max_conc_day;
  vector[N_groups] raw_between_log_max_conc_day;
  vector[N_indiv] raw_within_log_max_conc_day;
  real<lower = 0> tau_log_max_conc_day;
  
  real mu_log_shape;
  real<lower = 0> sigma_between_log_shape;
  vector<lower = 0>[N_groups] sigma_within_log_shape;
  vector[N_groups] raw_between_log_shape;
  vector[N_indiv] raw_within_log_shape;
  real<lower = 0> tau_log_shape;
  
  real<lower = 0> sigma_initial_conc;
  vector[N_indiv] initial_conc;
  
  real<lower = 0> sigma;
  
}
model{
  vector[N] y_hat;
  vector[N_indiv] max_conc;
  vector[N_indiv] log_max_conc_day;
  vector[N_indiv] shape;
  
  mu_log_max_conc ~ skew_normal(0, 1.5, -2);
  // mu_initial_conc ~ normal(0, 3);
  mu_log_max_conc_day ~ normal(log(7), 1);
  mu_log_shape ~ normal(1, 1);
  
  sigma_between_log_max_conc ~ skew_normal(0, 0.5, -2);
  sigma_initial_conc ~ normal(0, 1);
  sigma_between_log_max_conc_day ~ normal(0, 5);
  sigma_between_log_shape ~ normal(0, 2);
  
  sigma_within_log_max_conc ~ normal(0, tau_log_max_conc);
  sigma_within_log_max_conc_day ~ normal(0, tau_log_max_conc_day);
  sigma_within_log_shape ~ normal(0, tau_log_shape);
  
  tau_log_max_conc ~ normal(0, 0.3);
  tau_log_max_conc_day ~ normal(0, 1);
  tau_log_shape ~ normal(0, 0.3);
  
  raw_between_log_max_conc ~ normal(0, 1);
  raw_between_log_max_conc_day ~ normal(0, 1);
  raw_between_log_shape ~ normal(0, 1);
  
  raw_within_log_max_conc ~ normal(0, 1);
  raw_within_log_max_conc_day ~ normal(0, 1);
  raw_within_log_shape ~ normal(0, 1);
  
  initial_conc ~ normal(starts, sigma_initial_conc);
  
  sigma ~ normal(0, 0.5);
  
  max_conc = exp(mu_log_max_conc +
                  raw_between_log_max_conc[group_ID] * sigma_between_log_max_conc +
                  raw_within_log_max_conc .* sigma_within_log_max_conc[group_ID]);
  log_max_conc_day = mu_log_max_conc_day +
                  raw_between_log_max_conc_day[group_ID] * sigma_between_log_max_conc_day +
                  raw_within_log_max_conc_day .* sigma_within_log_max_conc_day[group_ID];
  shape = exp(mu_log_shape +
                raw_between_log_shape[group_ID] * sigma_between_log_shape +
                raw_within_log_shape .* sigma_within_log_shape[group_ID]);

  for(n in 1:N)
    y_hat[n] = initial_conc[indiv_ID[n]] + max_conc[indiv_ID[n]] * exp(-(((log(x[n]) - log_max_conc_day[indiv_ID[n]]) ^ 2) / shape[indiv_ID[n]]));

  if(fit == 1)
    y ~ normal(y_hat, sigma);
}
generated quantities{
  vector[N_indiv] max_conc_day;
  vector[N] log_lik;
  vector[N_indiv] max_conc;
  vector[N_indiv] log_max_conc_day;
  vector[N_indiv] shape;
  
  max_conc = exp(mu_log_max_conc +
                  raw_between_log_max_conc[group_ID] * sigma_between_log_max_conc +
                  raw_within_log_max_conc .* sigma_within_log_max_conc[group_ID]);
  log_max_conc_day = mu_log_max_conc_day +
                  raw_between_log_max_conc_day[group_ID] * sigma_between_log_max_conc_day +
                  raw_within_log_max_conc_day .* sigma_within_log_max_conc_day[group_ID];
  shape = exp(mu_log_shape +
                raw_between_log_shape[group_ID] * sigma_between_log_shape +
                raw_within_log_shape .* sigma_within_log_shape[group_ID]);

  max_conc_day = exp(log_max_conc_day);
  for(n in 1:N)
    log_lik[n] = normal_lpdf(y[n] | initial_conc[indiv_ID[n]] + max_conc[indiv_ID[n]] * exp(-(((log(x[n]) - log_max_conc_day[indiv_ID[n]]) ^ 2) / shape[indiv_ID[n]])), sigma);
}
