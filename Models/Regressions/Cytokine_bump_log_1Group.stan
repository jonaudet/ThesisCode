data{
  int N;  //Number of weights
  int N_indiv; //Number of inidividuals
  vector[N] y; //weights
  vector[N] x; //Times
  int<lower = 1, upper = N_indiv> indiv_ID[N]; //Individual to whom the observation belongs
  real max_conc_width;
}
parameters{
  real mu_log_max_conc;
  real<lower = 0> sigma_log_max_conc;
  vector[N_indiv] raw_log_max_conc;
  
  real mu_log_max_conc_day;
  real<lower = 0> sigma_log_max_conc_day;
  vector[N_indiv] raw_log_max_conc_day;
  
  real mu_log_shape;
  real<lower = 0> sigma_log_shape;
  vector[N_indiv] raw_log_shape;
  
  real mu_initial_conc;
  real<lower = 0> sigma_initial_conc;
  vector[N_indiv] raw_initial_conc;
  
  real<lower = 0> sigma;
  
}
model{
  vector[N] y_hat;
  vector[N_indiv] max_conc;
  vector[N_indiv] initial_conc;
  vector[N_indiv] log_max_conc_day;
  vector[N_indiv] shape;
  
  mu_log_max_conc ~ normal(0, 3);
  mu_initial_conc ~ normal(0, 3);
  mu_log_max_conc_day ~ normal(log(7), 1);
  mu_log_shape ~ normal(0, 1);
  
  sigma_log_max_conc ~ normal(0, 1);
  sigma_initial_conc ~ normal(0, 1);
  sigma_log_max_conc_day ~ normal(0, 1);
  sigma_log_shape ~ normal(0, 1);
  
  raw_log_max_conc ~ normal(0, 1);
  raw_initial_conc ~ normal(0, 1);
  raw_log_max_conc_day ~ normal(0, 1);
  raw_log_shape ~ normal(0, 1);
  
  sigma ~ normal(0, 0.5);
  
  max_conc = exp(mu_log_max_conc + raw_log_max_conc * sigma_log_max_conc);
  initial_conc = mu_initial_conc + raw_initial_conc * sigma_initial_conc;
  log_max_conc_day = mu_log_max_conc_day + raw_log_max_conc_day * sigma_log_max_conc_day;
  shape = exp(mu_log_shape + raw_log_shape * sigma_log_shape);

  for(n in 1:N)
    y_hat[n] = initial_conc[indiv_ID[n]] + max_conc[indiv_ID[n]] * exp(-(((log(x[n]) - log_max_conc_day[indiv_ID[n]]) ^ 2) / shape[indiv_ID[n]]));

  y ~ normal(y_hat, sigma);
}
generated quantities{
  vector[N_indiv] max_conc_day;
  vector[N] log_lik;
  vector[N_indiv] max_conc;
  vector[N_indiv] initial_conc;
  vector[N_indiv] log_max_conc_day;
  vector[N_indiv] shape;
  
  max_conc = exp(mu_log_max_conc + raw_log_max_conc * sigma_log_max_conc);
  initial_conc = mu_initial_conc + raw_initial_conc * sigma_initial_conc;
  log_max_conc_day = mu_log_max_conc_day + raw_log_max_conc_day * sigma_log_max_conc_day;
  shape = exp(mu_log_shape + raw_log_shape * sigma_log_shape);

  max_conc_day = exp(mu_log_max_conc_day + raw_log_max_conc_day * sigma_log_max_conc_day);
  for(n in 1:N)
    log_lik[n] = normal_lpdf(y[n] | initial_conc[indiv_ID[n]] + max_conc[indiv_ID[n]] * exp(-(((log(x[n]) - log_max_conc_day[indiv_ID[n]]) ^ 2) / shape[indiv_ID[n]])), sigma);
}
