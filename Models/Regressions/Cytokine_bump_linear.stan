data{
  int N;  //Number of weights
  vector[N] y; //weights
  vector[N] x; //Times
  real max_conc_width;
}
parameters{
  real max_conc;
  real log_max_conc_day;
  real<lower = 0> shape;
  real<lower = 0> sigma;
  real daily_change;
  real<lower = 0> initial_conc;
}
transformed parameters{
  vector[N] y_hat;

  for(n in 1:N)
    y_hat[n] = initial_conc + daily_change * x[n] + max_conc * exp(-(((log(x[n]) - log_max_conc_day) ^ 2) / shape));
}
model{
  max_conc ~ normal(0, max_conc_width);
  initial_conc ~ normal(0, 4);
  log_max_conc_day ~ uniform(0, log(max(x)));
  shape ~ normal(0, 1);
  sigma ~ normal(0, 1);
  daily_change ~ normal(0, 1);

  y ~ normal(y_hat, sigma);
}
generated quantities{
  real max_conc_day;
  vector[N] log_lik;

  max_conc_day = exp(log_max_conc_day);
  for(n in 1:N)
    log_lik[n] = normal_lpdf(y[n] | y_hat[n], sigma);
}
