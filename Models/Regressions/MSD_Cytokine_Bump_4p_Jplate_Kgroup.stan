data{
  // Data for estimation of concentration
  
  
  int N;      //Total number of wells measured
  int N_theta;  //Total number of independent samples (1 sample has multiple dilutions which have multiple replicates)
  int N_grp_dil;  //Total number of dilutions and groups (so that N_grp_dil X # of replicates == N)
  int J;      //Total number of plates
  int N_bot; //Number of values at Conc = 0

  int<lower = 1, upper = N_theta> uID[N_grp_dil]; //Sample ID for each dilution
  int<lower = 1, upper = J> pID[N_grp_dil];     //Plate ID for each dilution
  int<lower = 1, upper = N_grp_dil> dil_ID[N];  //Dilution ID for each well
  vector<lower = 1>[N_grp_dil] Dil_order;

  vector[N] meas_OD;    //Optical density measured for each well
  vector[N_grp_dil] dilution;  //Dilution factor for each dilution

  real mu_Std;       //Concentration of the standard
  real<lower = 0> sigma_std;    //Uncertainty on the concentration of the standard

  vector[N_bot] zeroes;
  int plate_zeroes[N_bot];

  real inflec_mu;
  
  int N_mix;
  vector[N_mix] means;
  
  int std_loc;
  
  
  // Data for modelling of concentrations
  
  int N_indiv; //Number of inidividuals
  int N_groups;
  int N_times;
  vector[N_times] x; //Times
  int<lower = 1, upper = N_indiv> indiv_ID[N]; //Individual to whom the observation belongs
  int<lower = 1, upper = N_groups> group_ID[N_indiv];
  int<lower = 1, upper = N_theta> time_uID[N_times]; //log_theta associated with timepoint
  int<lower = 1, upper = N_theta> d_0_uID[N_times]; //Day 0 log_theta associated with each timepoint
  int<lower = 0, upper = 1> fit;
  
  vector[N_indiv] starts;
}
parameters{
  // Parameters for estimation of concentration
  real<lower = 0> sigma_y_4p;

  real mu_Bottom;
  real mu_Span;
  real mu_log_Inflec;
  real mu_log_Slope;

  real<lower = 0> sigma_Bottom;
  real<lower = 0> sigma_Span;
  real<lower = 0> sigma_log_Inflec;
  real<lower = 0> sigma_log_Slope;

  vector[J] raw_Bottom;
  vector[J] raw_Span;
  vector[J] raw_log_Inflec;
  vector[J] raw_log_Slope;

  real std_raw;

  vector[N_grp - 1] log_theta;
  vector[N_grp_dil] log_x;
  
  
  // Parameters for modelling of concentration
  
  real mu_log_max_conc;
  real<lower = 0> sigma_between_log_max_conc;
  vector<lower = 0>[N_groups] sigma_within_log_max_conc;
  vector[N_groups] raw_between_log_max_conc;
  vector[N_indiv] raw_within_log_max_conc;
  real<lower = 0> tau_log_max_conc;
  
  real mu_log_max_conc_day;
  real<lower = 0> sigma_between_log_max_conc_day;
  vector<lower = 0>[N_groups] sigma_within_log_max_conc_day;
  vector[N_groups] raw_between_log_max_conc_day;
  vector[N_indiv] raw_within_log_max_conc_day;
  real<lower = 0> tau_log_max_conc_day;
  
  real mu_log_shape;
  real<lower = 0> sigma_between_log_shape;
  vector<lower = 0>[N_groups] sigma_within_log_shape;
  vector[N_groups] raw_between_log_shape;
  vector[N_indiv] raw_within_log_shape;
  real<lower = 0> tau_log_shape;
  
  real<lower = 0> sigma_y_model;
  
}
model{
  vector[N_grp_dil] log_conc;
  vector[J] Bottom;
  vector[J] Span;
  vector[J] log_Inflec;
  vector[J] Slope;
  vector[N_times] y_hat;
  vector[N_indiv] max_conc;
  vector[N_indiv] log_max_conc_day;
  vector[N_indiv] shape;

  // Priors for estimation of concentration
  sigma_y_4p ~ normal(0, 1);
  mu_Span ~ normal(10, 2);
  mu_Bottom ~ normal(5, 1);
  mu_log_Inflec ~ normal(inflec_mu, 2);
  mu_log_Slope ~ normal(0, 0.12);

  sigma_Bottom ~ normal(0, 1);
  sigma_Span ~ normal(0, 1);
  sigma_log_Inflec ~ normal(0, 1);
  sigma_log_Slope ~ normal(0, 0.3);

  raw_Bottom ~ normal(0, 1);
  raw_Span ~ normal(0, 1);
  raw_log_Inflec ~ normal(0, 1);
  raw_log_Slope ~ normal(0, 1);

  std_raw ~ normal(0, 1);

  log_conc ~ normal(log_x, 0.05 + 0.01 * Dil_order);

  target += log(sigma_std) - log(sigma_std * std_raw + mu_Std);
  
  // Priors for modelling of concentration
  
  mu_log_max_conc ~ normal(0, 3);
  mu_log_max_conc_day ~ normal(log(7), 1);
  mu_log_shape ~ normal(0, 1);
  
  sigma_between_log_max_conc ~ normal(0, 5);
  sigma_initial_conc ~ normal(0, 1);
  sigma_between_log_max_conc_day ~ normal(0, 5);
  sigma_between_log_shape ~ normal(0, 5);
  
  sigma_within_log_max_conc ~ normal(0, tau_log_max_conc);
  sigma_within_log_max_conc_day ~ normal(0, tau_log_max_conc_day);
  sigma_within_log_shape ~ normal(0, tau_log_shape);
  
  tau_log_max_conc ~ normal(0, 0.5);
  tau_log_max_conc_day ~ normal(0, 0.5);
  tau_log_shape ~ normal(0, 0.5);
  
  raw_between_log_max_conc ~ normal(0, 1);
  raw_between_log_max_conc_day ~ normal(0, 1);
  raw_between_log_shape ~ normal(0, 1);
  
  raw_within_log_max_conc ~ normal(0, 1);
  raw_within_log_max_conc_day ~ normal(0, 1);
  raw_within_log_shape ~ normal(0, 1);
  
  sigma_y_model ~ normal(0, 0.5);
  
  // Prior on log_theta (likelihood for modelling of concentration)
  
  max_conc = exp(mu_log_max_conc +
                  raw_between_log_max_conc[group_ID] * sigma_between_log_max_conc +
                  raw_within_log_max_conc .* sigma_within_log_max_conc[group_ID]);
  log_max_conc_day = mu_log_max_conc_day +
                  raw_between_log_max_conc_day[group_ID] * sigma_between_log_max_conc_day +
                  raw_within_log_max_conc_day .* sigma_within_log_max_conc_day[group_ID];
  shape = exp(mu_log_shape +
                raw_between_log_shape[group_ID] * sigma_between_log_shape +
                raw_within_log_shape .* sigma_within_log_shape[group_ID]);
  for(n in 1:N_times)
    log_theta[time_uID[n]] ~ normal(log_theta[d_0_uID[n]] + max_conc[indiv_ID[n]] * exp(-(((log(x[n]) - log_max_conc_day[indiv_ID[n]]) ^ 2) / shape[indiv_ID[n]])), sigma_y_model);
  
  // Likelihood for estimation of concentration
  
  for(i in 1:N_grp_dil){
    if(uID[i] == std_loc){
      log_conc[i] = (log(mu_Std + sigma_std * std_raw) + log_dilution[i]);
    } else {
      log_conc[i] = (log_theta[uID[i]] + log_dilution[i]);
    }
  }

  Bottom = mu_Bottom + sigma_Bottom * raw_Bottom;
  Span = mu_Span + sigma_Span * raw_Span;
  log_Inflec = mu_log_Inflec + sigma_log_Inflec * raw_log_Inflec;
  Slope = exp(mu_log_Slope + sigma_log_Slope * raw_log_Slope);

  meas_OD ~ normal(Bottom[pID[dil_ID]] + Span[pID[dil_ID]] .* exp(-log1p_exp(-(log_x[dil_ID] - log_Inflec[pID[dil_ID]]) .* Slope[pID[dil_ID]])), sigma_y_4p);

  zeroes ~ normal(Bottom[plate_zeroes], sigma_y);
  
  
  
}
