data{
int N;
int N_subjects;
vector[N] times;
vector[N] conc_iv;
int<lower = 1, upper = N_subjects> subj_ID[N];
}
parameters{
//means
real log_t_05_mu; // expressed as ln(2)/t_05 so elimination first then distribution
real log_t_IN_mu;
real log_V_d_mu;
real log_max_produced_mu;
//SDs
real<lower = 0> t_05_sigma;
real<lower = 0> t_IN_sigma;
real<lower = 0> V_d_sigma;
real<lower = 0> log_max_produced_sigma;
//Raw values
vector[N_subjects] t_elim_raw;
vector[N_subjects] t_IN_raw;
vector[N_subjects] V_d_raw;
vector[N_subjects] log_max_produced_raw;

vector<lower = 0>[N_subjects] delay;
vector[N_subjects] baseline;

real<lower = 1> df;
real<lower = 0> dispersion;

//Mixing parameters
real<lower = 0> mix_success;
real<lower = 0> mix_fail;
vector<lower = 0, upper = 1>[N_subjects] props;

}
transformed parameters{
vector<lower = 0>[N_subjects] t_elim;
vector<lower = 0>[N_subjects] t_IN;
vector<lower = 0>[N_subjects] V_d;
vector<lower = 0>[N_subjects] max_produced;

vector[N] calc_iv;
vector[N] calc_stable;

t_elim = exp(log_t_05_mu + t_elim_raw * t_05_sigma);
t_IN = exp(log_t_IN_mu + t_IN_raw * t_IN_sigma);
V_d = exp(log_V_d_mu + V_d_raw * V_d_sigma);
max_produced = exp(log_max_produced_mu + log_max_produced_raw * log_max_produced_sigma);

t_elim = log(2) ./ t_elim;

for(i in 1:N){
  real input;
  
  input = (max_produced[subj_ID[i]] * (1 - exp(-((times[i] - delay[subj_ID[i]]) * t_IN[subj_ID[i]])))) / V_d[subj_ID[i]];
  if(times[i] >= delay[subj_ID[i]]){
    calc_iv[i] = log(exp(baseline[subj_ID[i]]) + (input) * exp(-((times[i] - delay[subj_ID[i]]) * t_elim[subj_ID[i]])));
  } else {
    calc_iv[i] = baseline[subj_ID[i]];
  }
}

calc_stable = baseline[subj_ID];

}
model{
log_t_05_mu ~ normal(2, 2);
log_t_IN_mu ~ normal(-3, 2);
log_V_d_mu ~ normal(6, 1);
log_max_produced_mu ~ normal(16, 3);

//SDs
t_05_sigma ~ normal(0, 1);
t_IN_sigma ~ normal(0, 1);
V_d_sigma ~ normal(0, 1);
log_max_produced_sigma ~ normal(0, 3);

//Raw values
t_elim_raw ~ normal(0, 1);
t_IN_raw ~ normal(0, 1);
V_d_raw ~ normal(0, 1);
log_max_produced_raw ~ normal(0, 1);

delay ~ normal(0, 2);
baseline ~ normal(0, 4);

df ~ exponential(1.0 / 20.0);
dispersion ~ normal(0, 1);

//Mixture
mix_success ~ student_t(3, 0, 10);
mix_fail ~ student_t(3, 0, 10);
props ~ beta(mix_success, mix_fail);

for(i in 1:N)
target += log_mix(props[subj_ID[i]],
                    student_t_lpdf(conc_iv[i] | df, calc_iv[i], dispersion),
                    student_t_lpdf(conc_iv[i] | df, calc_stable[i], dispersion));
}
generated quantities{
vector[N_subjects] t_elim_lin;

for(i in 1:N_subjects){
  t_elim_lin[i] = log(2) / t_elim[i];
}
}
