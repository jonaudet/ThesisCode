data{
  int N;  //Number of weights
  vector[N] y; //weights
  vector[N] x; //Times
  real sigma_pri;
}
transformed data{
  vector[3] dir_pri = rep_vector(1, 3);
}
parameters{
  real<lower = 0> max_conc;
  real<lower = 0> min_conc;
  real log_max_conc_day_up;
  real log_max_conc_day_down;
  real<lower = 0> shape_up;
  real<lower = 0> shape_down;
  real<lower = 0> sigma;
  real daily_change;
  real initial_conc;
  real overall_mean;
  simplex[3] probs;
}
transformed parameters{
  vector[N] y_hat_up;
  vector[N] y_hat_down;

  for(n in 1:N){
    y_hat_up[n] = initial_conc + daily_change * x[n] + max_conc * exp(-(((log(x[n]) - log_max_conc_day_up) ^ 2) / shape_up));
    y_hat_down[n] = initial_conc + daily_change * x[n] - min_conc * exp(-(((log(x[n]) - log_max_conc_day_down) ^ 2) / shape_down));
  }
}
model{
  max_conc ~ gamma(4, 0.4);
  min_conc ~ gamma(4, 0.4);
  initial_conc ~ normal(0, 5);
  log_max_conc_day_up ~ normal(log(14), log(14));
  log_max_conc_day_down ~ normal(log(14), log(14));
  shape_up ~ normal(0, 1);
  shape_down ~ normal(0, 1);
  sigma ~ normal(0, sigma_pri);
  daily_change ~ normal(0, 1);
  
  overall_mean ~ normal(0, 5);
  
  probs ~ dirichlet(dir_pri);

  for(n in 1:N){
    vector[3] log_probs;
    log_probs[1] = log(probs[1]) + normal_lpdf(y[n] | overall_mean, sigma);
    log_probs[2] = log(probs[2]) + normal_lpdf(y[n] | y_hat_up, sigma);
    log_probs[3] = log(probs[3]) + normal_lpdf(y[n] | y_hat_down, sigma);
    target += log_sum_exp(log_probs);
  }
}
generated quantities{
  real max_conc_day_up;
  real max_conc_day_down;
  vector[N] log_lik;

  max_conc_day_up = exp(log_max_conc_day_up);
  max_conc_day_down = exp(log_max_conc_day_down);
  for(n in 1:N){
    vector[3] log_probs;
    log_probs[1] = log(probs[1]) + normal_lpdf(y[n] | overall_mean, sigma);
    log_probs[2] = log(probs[2]) + normal_lpdf(y[n] | y_hat_up, sigma);
    log_probs[3] = log(probs[3]) + normal_lpdf(y[n] | y_hat_down, sigma);
    log_lik[n] = log_sum_exp(log_probs);
  }
}
