data{
  int N;  //Number of Concentrations
  int N_indiv; //Number of inidividuals
  int N_groups;
  vector[N] y; //Concentration
  vector[N] x; //Times
  int<lower = 1, upper = N_indiv> indiv_ID[N]; //Individual to whom the observation belongs
  int<lower = 1, upper = N_groups> group_ID[N_indiv];
  int<lower = 0, upper = 1> fit;
}
parameters{
  real<lower = 0> sigma;
  real<lower = 1> df;
  
  real mu_log_max_conc;
  real<lower = 0> sigma_between_log_max_conc;
  vector<lower = 0>[N_groups] sigma_within_log_max_conc;
  vector[N_groups] raw_between_log_max_conc;
  vector[N_indiv] raw_within_log_max_conc;
  real<lower = 0> tau_log_max_conc;
  
  real mu_log_max_conc_day;
  real<lower = 0> sigma_between_log_max_conc_day;
  vector<lower = 0>[N_groups] sigma_within_log_max_conc_day;
  vector[N_groups] raw_between_log_max_conc_day;
  vector[N_indiv] raw_within_log_max_conc_day;
  real<lower = 0> tau_log_max_conc_day;
  
  real mu_log_shape;
  real<lower = 0> sigma_between_log_shape;
  vector<lower = 0>[N_groups] sigma_within_log_shape;
  vector[N_groups] raw_between_log_shape;
  vector[N_indiv] raw_within_log_shape;
  real<lower = 0> tau_log_shape;
  
  real mu_skew;
  real<lower = 0> sigma_between_skew;
  vector<lower = 0>[N_groups] sigma_within_skew;
  vector[N_groups] raw_between_skew;
  vector[N_indiv] raw_within_skew;
  real<lower = 0> tau_skew;
  
  real mu_initial_conc;
  real<lower = 0> sigma_initial_conc;
  vector[N_indiv] initial_conc;
}
model{
  vector[N] y_hat;
  vector[N_indiv] max_conc;
  vector[N_indiv] max_conc_day;
  vector[N_indiv] shape;
  vector[N_indiv] skew;
  
  mu_log_max_conc ~ normal(0, 1);
  mu_initial_conc ~ normal(0, 3);
  mu_log_max_conc_day ~ normal(log(7), 1);
  mu_log_shape ~ normal(6, 2);
  mu_skew ~ normal(0, 1);
  
  sigma_between_log_max_conc ~ normal(0, 1);
  sigma_initial_conc ~ normal(0, 1);
  sigma_between_log_max_conc_day ~ normal(0, 3);
  sigma_between_log_shape ~ normal(0, 2);
  sigma_between_skew ~ normal(0, 1);
  
  sigma_within_log_max_conc ~ normal(0, tau_log_max_conc);
  sigma_within_log_max_conc_day ~ normal(0, tau_log_max_conc_day);
  sigma_within_log_shape ~ normal(0, tau_log_shape);
  sigma_within_skew ~ normal(0, tau_skew);
  
  tau_log_max_conc ~ normal(0, 0.3);
  tau_log_max_conc_day ~ normal(0, 1);
  tau_log_shape ~ normal(0, 0.3);
  tau_skew ~ normal(0, 1);
  
  raw_between_log_max_conc ~ normal(0, 1);
  raw_between_log_max_conc_day ~ normal(0, 1);
  raw_between_log_shape ~ normal(0, 1);
  raw_between_skew ~ normal(0, 1);
  
  raw_within_log_max_conc ~ normal(0, 1);
  raw_within_log_max_conc_day ~ normal(0, 1);
  raw_within_log_shape ~ normal(0, 1);
  raw_within_skew ~ normal(0, 1);
  
  initial_conc ~ normal(mu_initial_conc, sigma_initial_conc);
  
  sigma ~ normal(0, 0.5);
  
  df ~ exponential(1 / 20.0);
  
  max_conc = exp(mu_log_max_conc +
                  raw_between_log_max_conc[group_ID] * sigma_between_log_max_conc +
                  raw_within_log_max_conc .* sigma_within_log_max_conc[group_ID]);
  max_conc_day = exp(mu_log_max_conc_day +
                  raw_between_log_max_conc_day[group_ID] * sigma_between_log_max_conc_day +
                  raw_within_log_max_conc_day .* sigma_within_log_max_conc_day[group_ID]);
  shape = exp(mu_log_shape +
                raw_between_log_shape[group_ID] * sigma_between_log_shape +
                raw_within_log_shape .* sigma_within_log_shape[group_ID]);
  skew = mu_skew +
                raw_between_skew[group_ID] * sigma_between_skew +
                raw_within_skew .* sigma_within_skew[group_ID];

  for(n in 1:N)
    y_hat[n] = initial_conc[indiv_ID[n]] +
      max_conc[indiv_ID[n]] * exp(-(((x[n] - max_conc_day[indiv_ID[n]]) ^ 2) / shape[indiv_ID[n]])) * Phi(skew[indiv_ID[n]] * ((x[n] - max_conc_day[indiv_ID[n]]) / sqrt(shape[indiv_ID[n]])));

  if(fit == 1)
    y ~ student_t(df, y_hat, sigma);
}
generated quantities{
  vector[N_indiv] max_conc_day;
  vector[N] log_lik;
  vector[N_indiv] max_conc;
  vector[N_indiv] shape;
  vector[N_indiv] skew;
  
  max_conc = exp(mu_log_max_conc +
                  raw_between_log_max_conc[group_ID] * sigma_between_log_max_conc +
                  raw_within_log_max_conc .* sigma_within_log_max_conc[group_ID]);
  max_conc_day = exp(mu_log_max_conc_day +
                  raw_between_log_max_conc_day[group_ID] * sigma_between_log_max_conc_day +
                  raw_within_log_max_conc_day .* sigma_within_log_max_conc_day[group_ID]);
  shape = exp(mu_log_shape +
                raw_between_log_shape[group_ID] * sigma_between_log_shape +
                raw_within_log_shape .* sigma_within_log_shape[group_ID]);
  skew = mu_skew +
                raw_between_skew[group_ID] * sigma_between_skew +
                raw_within_skew .* sigma_within_skew[group_ID];
  for(n in 1:N)
    log_lik[n] = student_t_lpdf(y[n] | df, initial_conc[indiv_ID[n]] +
      max_conc[indiv_ID[n]] * exp(-(((x[n] - max_conc_day[indiv_ID[n]]) ^ 2) / shape[indiv_ID[n]])) * Phi(skew[indiv_ID[n]] * ((x[n] - max_conc_day[indiv_ID[n]]) / sqrt(shape[indiv_ID[n]]))), sigma);
}
