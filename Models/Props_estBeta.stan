functions {
  real dirichlet_multinomial_lpmf(int[] y, vector alpha) {
      real log_top;
      real log_bottom;
      real n;
      real alpha_0;
      int N;

      N = size(y);

      n = sum(y);
      alpha_0 = sum(alpha);

      log_top = log(n) + lbeta(alpha_0, n);
      log_bottom = 0;

      for(i in 1:N)
        if(y[i] > 0 && alpha[i] > 0)
          log_bottom = log_bottom + log(y[i]) + lbeta(alpha[i], y[i]);
      return(log_top - log_bottom);
  }
}
data{
  int N;
  int P;
  int Counts[N, P];
  int J;
  int<lower = 1, upper = J> group[N];
  vector<lower = 0>[P] prior;
}
parameters{
  simplex[P] alpha[J];
}
model{
  for(i in 1:J){
    // alpha[i, 1] ~ exponential(1.0 / 200.0);
    // alpha[i, 2:P] ~ exponential(10.0);
    alpha[i] ~ dirichlet(prior);
  }
  for(i in 1:N){
    //target += dirichlet_multinomial_lpmf(Counts[, i] | alpha[group[i]]);
    Counts[i] ~ multinomial(alpha[group[i]]);
  }
}
