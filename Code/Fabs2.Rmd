---
title: "First Fab Experiment"
author: "Jonathan Audet"
date: "6 mai 2016"
output: 
  pdf_document: 
    fig_height: 5
    fig_width: 7
    number_sections: yes
    toc: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(checkpoint)
checkpoint("2017-07-13", scanForPackages = F)
library(readxl)
library(readr)
library(ggplot2)
library(dplyr)
library(tidyr)
library(Hmisc)
```

# Initial graphs

```{r Load}
Weight_Score <- read_excel("../Data/Fab_Weight_Score.xlsx")

Weight <- Weight_Score %>%
  select(Antibody, Format, Day, contains("Weight")) %>%
  gather("Sub", "Weight", Weight_1:Weight_10) %>%
  separate(Sub, c("Rem", "Num"), sep = "_") %>%
  select(-Rem) %>%
  filter(!is.na(Weight)) %>%
  mutate(Num = paste(Format, Num, sep = "_"))

Score <- Weight_Score %>%
  select(Antibody, Format, Day, contains("Score")) %>%
  gather("Sub", "Score", Score_1:Score_10) %>%
  separate(Sub, c("Rem", "Num"), sep = "_") %>%
  select(-Rem) %>%
  filter(!is.na(Score)) %>%
  mutate(Num = paste(Format, Num, sep = "_"))

TTD <- Score %>%
  group_by(Antibody, Format, Num) %>%
  top_n(1, Day) %>%
  summarise(Outcome = ifelse(Score >= 3, 1, 0),
            Time = ifelse(Day == 14, 28, Day)) %>%
  ungroup

source("https://raw.githubusercontent.com/kmiddleton/rexamples/master/qplot_survival.R")

library(survival)

Su <- Surv(TTD$Time, event = TTD$Outcome)
TTD$Group <- TTD$Format
fit <- survfit(Su ~ TTD$Antibody + TTD$Group)
s_frame <- createSurvivalFrame(fit) %>%
  tbl_df %>%
  separate(strata, c("Antibody", "Group"), sep = ", ") %>%
  separate(Antibody, c("Rem1", "Antibody"), sep = "=") %>%
  separate(Group, c("Rem2", "Group"), sep = "=") %>%
  select(-Rem1, -Rem2, -upper, -lower, -n.risk, -n.event, -n.censor) %>%
  mutate(Antibody = gsub("^\\s+|\\s+$", "", Antibody),
         Group = gsub("^\\s+|\\s+$", "", Group))

summary(fit)
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "1H3")
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "2G4")
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "4G7")

ggplot(s_frame, aes(time, surv * 100, colour = Group)) +
  geom_step(size = 1.1) +
  facet_wrap(~Antibody) +
  labs(x = "Time post-infection", y = "Survival") +
  theme_bw()
ggsave("../Results/Fabs2/Survival.pdf", width = 8, height = 6)
```

```{r Weight_graphs}
ggplot(Weight, aes(Day, Weight, group = Num, colour = Format)) +
  geom_point() +
  geom_line() +
  facet_wrap(~Antibody) +
  theme_bw()

ggplot(Weight, aes(Day, Weight, group = Num, colour = Format)) +
  facet_wrap(~Antibody) +
  geom_line(alpha = 0.4) +
  stat_summary(aes(group = NULL), fun.data = "mean_cl_boot", size = .65) +
  stat_summary(aes(group = NULL), fun.y = "mean", geom = "line", size = 1.1) +
  theme_bw()
ggsave("../Results/Fabs2/Weight.pdf", width = 8, height = 6)
```

```{r Weight_pct_grpahs}
Weight %>%
  group_by(Antibody, Format, Num) %>%
  arrange(Day) %>%
  mutate(Weight_change = Weight / Weight[1] * 100) %>%
  ungroup %>%
  ggplot(aes(Day, Weight_change, group = Num, colour = Format)) +
  geom_point() +
  geom_line() +
  facet_wrap(~Antibody) +
  theme_bw()

Weight %>%
  group_by(Antibody, Format, Num) %>%
  arrange(Day) %>%
  mutate(Weight_change = Weight / Weight[1] * 100) %>%
  ungroup %>%
  ggplot(aes(Day, Weight_change, group = Num, colour = Format)) +
  facet_wrap(~Antibody) +
  geom_line(alpha = 0.4) +
  stat_summary(aes(group = NULL), fun.data = "mean_cl_boot", size = 0.65) +
  stat_summary(aes(group = NULL), fun.y = "mean", geom = "line", size = 1.1) +
  theme_bw()
ggsave("../Results/Fabs2/Weight_Change.pdf", width = 8, height = 6)
```