---
title: "Survival Experiments"
author: "Jonathan Audet"
date: "11 Oct 2016"
output:
  html_document: 
    highlight: tango
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(checkpoint)
checkpoint("2017-07-13")
if(!require(dplyr)){
  install.packages("dplyr")
  library(dplyr)
}
if(!require(readr)){
  install.packages("readr")
  library(readr)
}
if(!require(readxl)){
  install.packages("readxl")
  library(readxl)
}
if(!require(tidyr)){
  install.packages("tidyr")
  library(tidyr)
}
if(!require(ggplot2)){
  install.packages("ggplot2")
  library(ggplot2)
}
if(!require(survival)){
  install.packages("survival")
  library(survival)
}


source("https://raw.githubusercontent.com/kmiddleton/rexamples/master/qplot_survival.R")

theme_set(theme_minimal() %+replace%
            theme(axis.title = element_text(size = 14, face = "bold"),
                  axis.text = element_text(size = 12),
                  strip.text = element_text(size = 14, face = "bold"),
                  panel.spacing.x = unit(0.05, "npc"),
                  panel.grid.major = element_line(colour = "grey75"),
                  panel.grid.minor = element_line(colour = "grey94"),
                  legend.position = "none"))
```

# Fabs

## Initial graphs

```{r Load}
Weight_Score <- read_excel("../Data/Fab_Weight_Score.xlsx")

Weight <- Weight_Score %>%
  select(Antibody, Format, Day, contains("Weight")) %>%
  gather("Sub", "Weight", Weight_1:Weight_10) %>%
  separate(Sub, c("Rem", "Num"), sep = "_") %>%
  select(-Rem) %>%
  filter(!is.na(Weight)) %>%
  mutate(Num = paste(Format, Num, sep = "_"),
         Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

Score <- Weight_Score %>%
  select(Antibody, Format, Day, contains("Score")) %>%
  gather("Sub", "Score", Score_1:Score_10) %>%
  separate(Sub, c("Rem", "Num"), sep = "_") %>%
  select(-Rem) %>%
  filter(!is.na(Score)) %>%
  mutate(Num = paste(Format, Num, sep = "_"))

TTD <- Score %>%
  group_by(Antibody, Format, Num) %>%
  top_n(1, Day) %>%
  summarise(Outcome = ifelse(Score >= 3, 1, 0),
            Time = ifelse(Day == 14, 28, Day)) %>%
  ungroup

Su <- Surv(TTD$Time, event = TTD$Outcome)
TTD$Group <- TTD$Format
fit <- survfit(Su ~ TTD$Antibody + TTD$Group)
s_frame <- createSurvivalFrame(fit) %>%
  tbl_df %>%
  separate(strata, c("Antibody", "Group"), sep = ", ") %>%
  separate(Antibody, c("Rem1", "Antibody"), sep = "=") %>%
  separate(Group, c("Rem2", "Group"), sep = "=") %>%
  select(-Rem1, -Rem2, -upper, -lower, -n.risk, -n.event, -n.censor) %>%
  mutate(Antibody = gsub("^\\s+|\\s+$", "", Antibody),
         Group = gsub("^\\s+|\\s+$", "", Group),
         Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

summary(fit)
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "1H3")
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "2G4")
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "4G7")

group_labels <- TTD %>%
  group_by(Antibody, Format) %>%
  dplyr::summarize(Fraction = (1 - sum(Outcome)/n()) + 0.08, Survival = 100 * Fraction, maxDay = max(Time)) %>%
  mutate(Day = ifelse(maxDay == 28, 26, maxDay + 3)) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

pdf("../Results/Fab_Survival.pdf", width = 8, height = 6, useDingbats = F)
ggplot(s_frame, aes(time, surv * 100, colour = Group)) +
  geom_step(size = 1.1) +
  geom_text(data = group_labels, aes(Day, Survival, label = Format, colour = Format), fontface = "bold", size = 4) +
  facet_wrap(~Antibody) +
  labs(x = "Days post-infection", y = "Survival") +
  scale_x_continuous(breaks = seq(0, 30, 7)) +
  scale_y_continuous(breaks = seq(0, 100, 25)) +
  scale_colour_manual(values = c("navyblue", "orange3", "red3"))
dev.off()
```

```{r Weight_pct_grpahs}
Weight_pct <- Weight %>%
  group_by(Antibody, Format, Num) %>%
  arrange(Day) %>%
  mutate(Weight_change = Weight / Weight[1] * 100) %>%
  ungroup

group_labels <- Weight_pct %>%
  group_by(Antibody, Format) %>%
  top_n(1, Day) %>%
  dplyr::summarize(Weight_change = mean(Weight_change), maxDay = max(Day)) %>%
  mutate(Day = ifelse(maxDay == 14, 15, maxDay + 1)) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

pdf("../Results/Fab_Weight_Change.pdf", width = 8, height = 6, useDingbats = F)
ggplot(Weight_pct, aes(Day, Weight_change, group = Num, colour = Format)) +
  facet_wrap(~Antibody) +
  geom_line(alpha = 0.2) +
  stat_summary(aes(group = NULL), fun.y = "mean", geom = "line", size = 1) +
  geom_text(data = group_labels, aes(Day, Weight_change, label = Format, colour = Format, group = NA), fontface = "bold", size = 4) +
  labs(x = "Days post-infection", y = "Weight (% of Day 1)") +
  scale_x_continuous(breaks = seq(0, 30, 7), limits = c(0, NA)) +
  scale_colour_manual(values = c("navyblue", "orange3", "red3"))
dev.off()
```

# C1q

## Initial graphs

```{r "Load_C1q"}
Weight_Score <- read_excel("../Data/C1q_Weight_Score.xlsx")

Weight <- Weight_Score %>%
  select(Antibody, C1q, Day, contains("Weight")) %>%
  gather("Sub", "Weight", Weight_1:Weight_6) %>%
  separate(Sub, c("Rem", "Num"), sep = "_") %>%
  select(-Rem) %>%
  filter(!is.na(Weight)) %>%
  mutate(Num = paste(C1q, Num, sep = "_"))

Score <- Weight_Score %>%
  select(Antibody, C1q, Day, contains("Score")) %>%
  gather("Sub", "Score", Score_1:Score_6) %>%
  separate(Sub, c("Rem", "Num"), sep = "_") %>%
  select(-Rem) %>%
  filter(!is.na(Score)) %>%
  mutate(Num = paste(C1q, Num, sep = "_"))

TTD <- Score %>%
  group_by(Antibody, C1q, Num) %>%
  top_n(1, Day) %>%
  summarise(Outcome = ifelse(Score >= 3, 1, 0),
            Time = ifelse(Day == 14, 28, Day)) %>%
  ungroup

Su <- Surv(TTD$Time, event = TTD$Outcome)
TTD$Group <- TTD$C1q
fit <- survfit(Su ~ TTD$Antibody + TTD$Group)
s_frame <- createSurvivalFrame(fit) %>%
  tbl_df %>%
  separate(strata, c("Antibody", "Group"), sep = ", ") %>%
  separate(Antibody, c("Rem1", "Antibody"), sep = "=") %>%
  separate(Group, c("Rem2", "Group"), sep = "=") %>%
  select(-Rem1, -Rem2, -upper, -lower, -n.risk, -n.event, -n.censor) %>%
  mutate(Antibody = gsub("^\\s+|\\s+$", "", Antibody),
         Group = gsub("^\\s+|\\s+$", "", Group),
         Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

summary(fit)
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "PBS")
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "1H3")
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "2G4")
survdiff(Su ~ Antibody + Group, data = TTD, subset = TTD$Antibody == "4G7")

group_labels <- TTD %>%
  group_by(Antibody, C1q) %>%
  dplyr::summarize(Fraction = (1 - sum(Outcome)/n()) + 0.08, Survival = 100 * Fraction, maxDay = max(Time)) %>%
  mutate(Day = ifelse(maxDay == 28, 26, maxDay + 3)) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

pdf("../Results/C1q_Survival.pdf", width = 8, height = 6, useDingbats = F)
ggplot(s_frame, aes(time, surv * 100, colour = Group)) +
  geom_step(size = 1.1) +
  geom_text(data = group_labels, aes(Day, Survival, label = C1q, colour = C1q), fontface = "bold", size = 4) +
  facet_wrap(~Antibody) +
  labs(x = "Days post-infection", y = "Survival") +
  scale_x_continuous(breaks = seq(0, 30, 7)) +
  scale_y_continuous(breaks = seq(0, 100, 25)) +
  scale_colour_manual(values = c("navyblue", "orange3", "red3"))
dev.off()
```

```{r C1q_Weight_pct_grpahs}
Weight_pct <- Weight %>%
  group_by(Antibody, C1q, Num) %>%
  arrange(Day) %>%
  mutate(Weight_change = Weight / Weight[1] * 100) %>%
  ungroup %>%
  mutate(Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

group_labels <- Weight_pct %>%
  group_by(Antibody, C1q) %>%
  top_n(1, Day) %>%
  dplyr::summarize(Weight_change = mean(Weight_change), maxDay = max(Day)) %>%
  mutate(Day = ifelse(maxDay == 14, 15, maxDay + 1)) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

pdf("../Results/C1q_Weight_Change.pdf", width = 8, height = 6, useDingbats = F)
ggplot(Weight_pct, aes(Day, Weight_change, group = Num, colour = C1q)) +
  facet_wrap(~Antibody) +
  geom_line(alpha = 0.2) +
  stat_summary(aes(group = NULL), fun.y = "mean", geom = "line", size = 1) +
  geom_text(data = group_labels, aes(Day, Weight_change, label = C1q, colour = C1q, group = NA), fontface = "bold", size = 4) +
  labs(x = "Days post-infection", y = "Weight (% of Day 1)") +
  scale_x_continuous(breaks = seq(0, 30, 7), limits = c(0, NA)) +
  scale_colour_manual(values = c("navyblue", "orange3", "red3"))
dev.off()
```

# C3 depleted

## Initial graphs

```{r Load_C3}
Weight <- read_excel("../Data/FcNKC3_weights.xlsx") %>%
  filter(Model == "C3", !is.na(Weight_pct)) %>%
  mutate(mAb = factor(mAb, levels = c("PBS", "1H3", "2G4", "4G7")))
TTD <- read_csv("../Data/FcNKC3.csv") %>%
  filter(Model == "C3")

Su <- Surv(TTD$Day, event = TTD$Event)
TTD$Group <- TTD$KO
fit <- survfit(Su ~ TTD$mAb + TTD$Group)
s_frame <- createSurvivalFrame(fit) %>%
  tbl_df %>%
  separate(strata, c("Antibody", "Group"), sep = ", ") %>%
  separate(Antibody, c("Rem1", "Antibody"), sep = "=") %>%
  separate(Group, c("Rem2", "Group"), sep = "=") %>%
  select(-Rem1, -Rem2, -upper, -lower, -n.risk, -n.event, -n.censor) %>%
  mutate(Antibody = gsub("^\\s+|\\s+$", "", Antibody),
         Group = gsub("^\\s+|\\s+$", "", Group),
         Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

summary(fit)
survdiff(Su ~ mAb + Group, data = TTD, subset = TTD$mAb == "1H3")
survdiff(Su ~ mAb + Group, data = TTD, subset = TTD$mAb == "2G4")
survdiff(Su ~ mAb + Group, data = TTD, subset = TTD$mAb == "4G7")

group_labels <- TTD %>%
  group_by(mAb, KO) %>%
  dplyr::summarize(Fraction = (1 - sum(Event)/n()) + 0.08, Survival = 100 * Fraction, maxDay = max(Day)) %>%
  mutate(Day = ifelse(maxDay == 28, 29, maxDay + 1),
         Antibody = mAb) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(Antibody = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

pdf("../Results/C3_Survival.pdf", width = 4, height = 9, useDingbats = F)
ggplot(s_frame, aes(time, surv * 100, colour = Group)) +
  geom_step(size = 1.1) +
  geom_text(data = group_labels, aes(Day, Survival, label = KO, colour = KO), fontface = "bold", size = 4) +
  facet_wrap(~Antibody, ncol = 1) +
  labs(x = "Days post-infection", y = "Survival") +
  scale_x_continuous(breaks = seq(0, 30, 7), limits = c(0, NA)) +
  scale_y_continuous(breaks = seq(0, 100, 25)) +
  scale_colour_manual(values = c("navyblue", "orange3"))
dev.off()
```

```{r C3_Weight_pct_grpahs}
group_labels <- Weight %>%
  group_by(mAb, KO) %>%
  top_n(1, Day) %>%
  dplyr::summarize(Weight_change = mean(Weight_pct), maxDay = max(Day)) %>%
  mutate(Day = ifelse(maxDay == 14, 15, maxDay + 1)) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(mAb = factor(mAb, levels = c("PBS", "1H3", "2G4", "4G7")))

pdf("../Results/C3_Weight_Change.pdf", width = 4, height = 9, useDingbats = F)
ggplot(Weight, aes(Day, Weight_pct, colour = KO)) +
  facet_wrap(~mAb, ncol = 1) +
  geom_line(alpha = 0.2) +
  stat_summary(aes(group = NULL), fun.y = "mean", geom = "line", size = 1) +
  geom_text(data = group_labels, aes(Day, Weight_change, label = KO, colour = KO, group = NA), fontface = "bold", size = 4) +
  labs(x = "Days post-infection", y = "Weight (% of Day 1)") +
  scale_x_continuous(breaks = seq(0, 30, 7), limits = c(0, NA)) +
  scale_colour_manual(values = c("navyblue", "orange3"))
dev.off()
```

# Common $\gamma$-chain KO

## Initial graphs

```{r Load_Fc}
Weight <- read_excel("../Data/FcNKC3_weights.xlsx") %>%
  filter(Model == "FcR", !is.na(Weight_pct)) %>%
  mutate(mAb = factor(mAb, levels = c("PBS", "1H3", "2G4", "4G7")))
TTD <- read_csv("../Data/FcNKC3.csv") %>%
  filter(Model == "FcR")

Su <- Surv(TTD$Day, event = TTD$Event)
TTD$Group <- TTD$KO
fit <- survfit(Su ~ TTD$mAb + TTD$Group)
s_frame <- createSurvivalFrame(fit) %>%
  tbl_df %>%
  separate(strata, c("Antibody", "Group"), sep = ", ") %>%
  separate(Antibody, c("Rem1", "Antibody"), sep = "=") %>%
  separate(Group, c("Rem2", "Group"), sep = "=") %>%
  select(-Rem1, -Rem2, -upper, -lower, -n.risk, -n.event, -n.censor) %>%
  mutate(Antibody = gsub("^\\s+|\\s+$", "", Antibody),
         Group = gsub("^\\s+|\\s+$", "", Group),
         mAb = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

summary(fit)
survdiff(Su ~ mAb + Group, data = TTD, subset = TTD$mAb == "1H3")
survdiff(Su ~ mAb + Group, data = TTD, subset = TTD$mAb == "2G4")
survdiff(Su ~ mAb + Group, data = TTD, subset = TTD$mAb == "4G7")

group_labels <- TTD %>%
  group_by(mAb, KO) %>%
  dplyr::summarize(Fraction = (1 - sum(Event)/n()) + 0.08, Survival = 100 * Fraction, maxDay = max(Day)) %>%
  mutate(Day = ifelse(maxDay == 28, 29, maxDay + 1),
         Antibody = mAb) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(mAb = factor(mAb, levels = c("PBS", "1H3", "2G4", "4G7")))

pdf("../Results/FcR_Survival.pdf", width = 4, height = 9, useDingbats = F)
ggplot(s_frame, aes(time, surv * 100, colour = Group)) +
  geom_step(size = 1.1) +
  geom_text(data = group_labels, aes(Day, Survival, label = KO, colour = KO), fontface = "bold", size = 4) +
  facet_wrap(~mAb, ncol = 1) +
  labs(x = "Days post-infection", y = "Survival") +
  scale_x_continuous(breaks = seq(0, 30, 7), limits = c(0, NA)) +
  scale_y_continuous(breaks = seq(0, 100, 25)) +
  scale_colour_manual(values = c("navyblue", "orange3"))
dev.off()
```

```{r Fc_Weight_pct_grpahs}
group_labels <- Weight %>%
  group_by(mAb, KO) %>%
  top_n(1, Day) %>%
  dplyr::summarize(Weight_change = mean(Weight_pct), maxDay = max(Day)) %>%
  mutate(Day = ifelse(maxDay == 14, 15, maxDay + 1)) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(mAb = factor(mAb, levels = c("PBS", "1H3", "2G4", "4G7")))

pdf("../Results/FcR_Weight_Change.pdf", width = 4, height = 9, useDingbats = F)
ggplot(Weight, aes(Day, Weight_pct, colour = KO)) +
  facet_wrap(~mAb, ncol = 1) +
  geom_line(alpha = 0.2) +
  stat_summary(aes(group = NULL), fun.y = "mean", geom = "line", size = 1) +
  geom_text(data = group_labels, aes(Day, Weight_change, label = KO, colour = KO, group = NA), fontface = "bold", size = 4) +
  labs(x = "Days post-infection", y = "Weight (% of Day 1)") +
  scale_x_continuous(breaks = seq(0, 30, 7), limits = c(0, NA)) +
  scale_colour_manual(values = c("navyblue", "orange3"))
dev.off()
```

# ${Nfil3}^{-/-}$ KO

## Initial graphs

```{r Load_C3}
Weight <- read_excel("../Data/FcNKC3_weights.xlsx") %>%
  filter(Model == "Nfil3", !is.na(Weight_pct)) %>%
  mutate(mAb = factor(mAb, levels = c("PBS", "1H3", "2G4", "4G7")))
TTD <- read_csv("../Data/FcNKC3.csv") %>%
  filter(Model == "NK")

Su <- Surv(TTD$Day, event = TTD$Event)
TTD$Group <- TTD$KO
fit <- survfit(Su ~ TTD$mAb + TTD$Group)
s_frame <- createSurvivalFrame(fit) %>%
  tbl_df %>%
  separate(strata, c("Antibody", "Group"), sep = ", ") %>%
  separate(Antibody, c("Rem1", "Antibody"), sep = "=") %>%
  separate(Group, c("Rem2", "Group"), sep = "=") %>%
  select(-Rem1, -Rem2, -upper, -lower, -n.risk, -n.event, -n.censor) %>%
  mutate(Antibody = gsub("^\\s+|\\s+$", "", Antibody),
         Group = gsub("^\\s+|\\s+$", "", Group),
         mAb = factor(Antibody, levels = c("PBS", "1H3", "2G4", "4G7")))

summary(fit)
survdiff(Su ~ mAb + Group, data = TTD, subset = TTD$mAb == "1H3")
survdiff(Su ~ mAb + Group, data = TTD, subset = TTD$mAb == "2G4")
survdiff(Su ~ mAb + Group, data = TTD, subset = TTD$mAb == "4G7")

group_labels <- TTD %>%
  group_by(mAb, KO) %>%
  dplyr::summarize(Fraction = (1 - sum(Event)/n()) + 0.08, Survival = 100 * Fraction, maxDay = max(Day)) %>%
  mutate(Day = ifelse(maxDay == 28, 29, maxDay + 1),
         Antibody = mAb) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(mAb = factor(mAb, levels = c("PBS", "1H3", "2G4", "4G7")))


pdf("../Results/NK_survival.pdf", width = 4, height = 9, useDingbats = F)
ggplot(s_frame, aes(time, surv * 100, colour = Group)) +
  geom_step(size = 1.1) +
  geom_text(data = group_labels, aes(Day, Survival, label = KO, colour = KO), fontface = "bold", size = 4) +
  facet_wrap(~mAb, ncol = 1) +
  labs(x = "Days post-infection", y = "Survival") +
  scale_x_continuous(breaks = seq(0, 30, 7), limits = c(0, NA)) +
  scale_y_continuous(breaks = seq(0, 100, 25)) +
  scale_colour_manual(values = c("navyblue", "orange3"))
dev.off()
```

```{r Weight_pct_grpahs_NK}
group_labels <- Weight %>%
  group_by(mAb, KO) %>%
  top_n(1, Day) %>%
  dplyr::summarize(Weight_change = mean(Weight_pct), maxDay = max(Day)) %>%
  mutate(Day = ifelse(maxDay == 14, 15, maxDay + 1)) %>%
  ungroup %>%
  select(-maxDay) %>%
  mutate(mAb = factor(mAb, levels = c("PBS", "1H3", "2G4", "4G7")))

pdf("../Results/Nfil3_Weight_Change.pdf", width = 4, height = 9, useDingbats = F)
ggplot(Weight, aes(Day, Weight_pct, colour = KO)) +
  facet_wrap(~mAb, ncol = 1) +
  geom_line(alpha = 0.2) +
  stat_summary(aes(group = NULL), fun.y = "mean", geom = "line", size = 1) +
  geom_text(data = group_labels, aes(Day, Weight_change, label = KO, colour = KO, group = NA), fontface = "bold", size = 4) +
  labs(x = "Days post-infection", y = "Weight (% of Day 1)") +
  scale_x_continuous(breaks = seq(0, 30, 7), limits = c(0, NA)) +
  scale_colour_manual(values = c("navyblue", "orange3"))
dev.off()
```