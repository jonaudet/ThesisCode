---
title: "H-13-003 Analysis of FACS proportions"
output: html_notebook
---

```{r setup}
library(checkpoint)
checkpoint("2017-07-13", scanForPackages = F)
library(MCMCpack)
library(dplyr)
library(readr)
library(tidyr)
library(rstan)
library(codetools)
library(rstudioapi)
library(bayesplot)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

HDI <- function(Values, Interval = 0.95){
  Values <- Values[!is.na(Values)]
  intSize <- as.integer(length(Values) * Interval)
  startMax <- as.integer(length(Values) - (length(Values) * Interval))
  ordered <- Values[sort.list(Values)]
  low <- 1
  diff <- Inf
  for(i in 1:startMax)
    if(ordered[i + intSize] - ordered[i] < diff){
      low <- i
      diff <- ordered[i + intSize] - ordered[i]
    }
  return(data.frame(LowerHDI = ordered[low], HigherHDI = ordered[low + intSize]))
}

logit <- function(x) log(x / (1 - x))

```


```{r ReadData}
June <- read_csv("../Results/H-13-003/June/Stim-GateCounts_simple.csv") %>%
  bind_rows(read_csv("../Results/H-13-003/June/Neg-GateCounts_simple.csv"))
Fall <- read_csv("../Results/H-13-003/fall/Stim-GateCounts_simple.csv") %>%
  bind_rows(read_csv("../Results/H-13-003/fall/Neg-GateCounts_simple.csv")) %>%
  inner_join(read_csv("../Data/H-13-003_fall NHP.csv"), by = c("Animal" = "NHP_ID"))
```

# CD4

```{r CleanData}
June <- mutate(June, Experiment = "June") %>%
  separate(Animal, c("Group", "ID"), sep = 1, remove = F)
Fall <- mutate(Fall, Experiment = "Fall") %>%
  separate(ExpID, c("Group", "ID"), sep = 1, remove = F)

Experiment <- bind_rows(June, Fall) %>%
  filter(Parent %in% c("CD4"),
         Stim != "PMA",
         !(Population %in% c("CD8pIFNg", "CD8pIL4", "CD8pIL2", "CD8pCD107a",
                             "CD4pIFNg", "CD4pIL4", "CD4pIL2", "CD4pCD107a",
                             "CD8Any", "CD4Any"))) %>%
  mutate(GroupID = as.numeric(factor(paste(Experiment, Group, Parent, Stim, Day))),
         PairID = as.numeric(factor(paste(Experiment, Group, Stim, Parent))))

Experiment2 <- Experiment %>%
  spread(Population, Count)

counts <- data.matrix(Experiment2[, 12:27])
```

```{r runModel}
mod <- stan_model(file = "../Models/Props_estBeta.stan")

res <- sampling(mod,
                data = list(N = nrow(Experiment2),
                            P = 16,
                            J = max(Experiment2$GroupID),
                            group = Experiment2$GroupID,
                            prior = rep(1, 16),
                            Counts = counts),
                refresh = 50, sample_file = "dia", control = list(adapt_delta = 0.9, max_treedepth = 10))
saveRDS(res, paste("../Results/H-13-003/Results", Sys.Date(), sep = "_"))
```

```{r Results}
probs <- rstan::extract(res, "alpha")$alpha

medians <- t(apply(probs[,,], 2, apply, 2, median))

Experiment2$Prop <- 0
Experiment3 <- Experiment2 %>%
  gather("Population", "Count", 12:27) %>%
  mutate(Pop_ID = as.numeric(factor(Population))) %>%
  distinct(Experiment, Group, Population, GroupID, Pop_ID, Stim, Prop)

Experiment3$Low <- 0
Experiment3$Top <- 0
for(i in 1:nrow(Experiment3)){
  Experiment3$Prop[i] <- medians[Experiment3$GroupID[i], Experiment3$Pop_ID[i]]
  Experiment3$Low[i] <- HDI(probs[, Experiment3$GroupID[i], Experiment3$Pop_ID[i]])$LowerHDI
  Experiment3$Top[i] <- HDI(probs[, Experiment3$GroupID[i], Experiment3$Pop_ID[i]])$HigherHDI
}

Experiment3$Logit_Prop <- logit(Experiment3$Prop)
Experiment3$Logit_low <- logit(Experiment3$Low)
Experiment3$Logit_top <- logit(Experiment3$Top)

ggplot(Experiment3, aes(Population, Logit_Prop, colour = Stim)) +
  geom_pointrange(aes(ymin = Logit_low, ymax = Logit_top)) +
  facet_grid(Experiment~Group)

Experiment3 <- Experiment3 %>%
  group_by(Experiment, Group) %>%
  arrange(Stim, Population) %>%
  mutate(Negative = GroupID[1]) %>%
  ungroup

Experiment3$Prob <- 0

for(i in 1:nrow(Experiment3)){
  prob <- sum(probs[, Experiment3$GroupID[i], Experiment3$Pop_ID[i]] > probs[, Experiment3$Negative[i], Experiment3$Pop_ID[i]]) / dim(probs)[1]
  Experiment3$Prob[i] <- prob
}

pops1 <- unique(Experiment3$Population)
pops2 <- pops1[pops1 != "All.Neg"]
temp <- data_frame(pop = pops2,
                   Num = str_count(pop, "\\.") + 1)
temp <- temp %>%
  arrange(desc(Num), pop)

pop_levels <- c(temp$pop, "All.Neg")

Experiment3 <- mutate(Experiment3, Stim = ifelse(Stim == "pp1", "Pool1",
                                                 ifelse(Stim == "pp2", "Pool2",
                                                        ifelse(Stim == "pp3", "Pool3", Stim))),
                      Population = factor(Population, levels = pop_levels))
Experiment3 %>%
  filter(Population != "All.Neg") %>%
  ggplot(aes(Population, Prop * 100, colour = Stim)) +
  geom_pointrange(aes(ymin = Low * 100, ymax = Top * 100, alpha = Prob)) +
  labs(x = "Population", y = "% of CD4") +
  facet_grid(Experiment~Group, scales = "free") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.2),
        panel.spacing.y = unit(1, "lines"))

hist(Experiment3$Prob, breaks = 100)

```

```{r Post_Pred}
PP_dat <- Experiment2 %>%
  dplyr::select(Experiment, Animal, GroupID, Stim, ParentCount)

pop_names <- names(Experiment2)[12:27]

PP <- data_frame(Experiment = rep(PP_dat$Experiment, each = 4000),
                 Animal = rep(PP_dat$Animal, each = 4000),
                 GroupID = rep(PP_dat$GroupID, each = 4000),
                 Stim = rep(PP_dat$Stim, each = 4000),
                 ParentCount = rep(PP_dat$ParentCount, each = 4000),
                 Sim = rep(1:4000, 112))

combs <- distinct(PP, Experiment, Animal, Stim)

for(i in 1:nrow(combs)){
  df <- PP %>%
    filter(Experiment == combs$Experiment[i], Animal == combs$Animal[i], Stim == combs$Stim[i])
  m <- matrix(0, nrow = nrow(df), ncol = 16)
  colnames(m) <- pop_names
  for(j in 1:nrow(df)){
    if(df$Sim[j] == 1)
      print(paste(df$Animal[j], df$Stim[j]))
    if((df$Sim[j] %% 200) == 0)
      print(df$Sim[j])
    m[j, ] <- rmultinom(1, df$ParentCount[j], probs[df$Sim[j], df$GroupID[j], ])
  }
  
  df <- bind_cols(df, as_data_frame(m))
  ref <- filter(Experiment2, Animal == combs$Animal[i], Stim == combs$Stim[i])
  df <- df %>%
    gather(Population, Count, 7:22)
  ref <- ref %>%
    gather(Population, Count, 12:27)
  p <- ggplot(df, aes(Count, fill = Population)) +
    geom_histogram() +
    geom_vline(data = ref, aes(xintercept = Count), colour = "red", size = 2) +
    facet_wrap(~Population, scales = "free") +
    theme_minimal()
  pdf(paste("../Results/H-13-003/", combs$Experiment[i], "_", combs$Animal[i], "_", combs$Stim[i], "_PP.pdf", sep = ""), height = 16, width = 16)
  plot(p)
  dev.off()
}

```

# CD8

```{r CleanData_CD8}
Experiment_CD8 <- bind_rows(June, Fall) %>%
  filter(Parent %in% c("CD8"),
         Stim != "PMA",
         !(Population %in% c("CD8pIFNg", "CD8pIL4", "CD8pIL2", "CD8pCD107a",
                             "CD4pIFNg", "CD4pIL4", "CD4pIL2", "CD4pCD107a",
                             "CD8Any", "CD4Any"))) %>%
  mutate(GroupID = as.numeric(factor(paste(Experiment, Group, Parent, Stim, Day))),
         PairID = as.numeric(factor(paste(Experiment, Group, Stim, Parent))))

Experiment2_CD8 <- Experiment_CD8 %>%
  spread(Population, Count)

counts_CD8 <- data.matrix(Experiment2_CD8[, 12:27])
```


```{r runModel_CD8}
mod <- stan_model(file = "../Models/Props_estBeta.stan")

res_CD8 <- sampling(mod,
                    data = list(N = nrow(Experiment2_CD8),
                                P = 16,
                                J = max(Experiment2_CD8$GroupID),
                                group = Experiment2_CD8$GroupID,
                                prior = rep(1, 16),
                                Counts = counts_CD8),
                    refresh = 50, sample_file = "dia", control = list(adapt_delta = 0.9, max_treedepth = 10))
saveRDS(res, paste("../Results/H-13-003/Results_CD8", Sys.Date(), sep = "_"))
```

```{r Results_CD8}
probs_CD8 <- rstan::extract(res_CD8, "alpha")$alpha

medians_CD8 <- t(apply(probs_CD8[,,], 2, apply, 2, median))

Experiment2_CD8$Prop <- 0
Experiment3_CD8 <- Experiment2_CD8 %>%
  gather("Population", "Count", 12:27) %>%
  mutate(Pop_ID = as.numeric(factor(Population))) %>%
  distinct(Experiment, Group, Population, GroupID, Pop_ID, Stim, Prop)

Experiment3_CD8$Low <- 0
Experiment3_CD8$Top <- 0
for(i in 1:nrow(Experiment3_CD8)){
  Experiment3_CD8$Prop[i] <- medians_CD8[Experiment3_CD8$GroupID[i], Experiment3_CD8$Pop_ID[i]]
  Experiment3_CD8$Low[i] <- HDI(probs_CD8[, Experiment3_CD8$GroupID[i], Experiment3_CD8$Pop_ID[i]])$LowerHDI
  Experiment3_CD8$Top[i] <- HDI(probs_CD8[, Experiment3_CD8$GroupID[i], Experiment3_CD8$Pop_ID[i]])$HigherHDI
}

Experiment3_CD8$Logit_Prop <- logit(Experiment3_CD8$Prop)
Experiment3_CD8$Logit_low <- logit(Experiment3_CD8$Low)
Experiment3_CD8$Logit_top <- logit(Experiment3_CD8$Top)

ggplot(Experiment3_CD8, aes(Population, Logit_Prop, colour = Stim)) +
  geom_pointrange(aes(ymin = Logit_low, ymax = Logit_top)) +
  facet_grid(Experiment~Group)

Experiment3_CD8 <- Experiment3_CD8 %>%
  group_by(Experiment, Group) %>%
  arrange(Stim, Population) %>%
  mutate(Negative = GroupID[1]) %>%
  ungroup

Experiment3_CD8$Prob <- 0

for(i in 1:nrow(Experiment3_CD8)){
  prob <- sum(probs_CD8[, Experiment3_CD8$GroupID[i], Experiment3_CD8$Pop_ID[i]] > probs_CD8[, Experiment3_CD8$Negative[i], Experiment3_CD8$Pop_ID[i]]) / dim(probs_CD8)[1]
  Experiment3_CD8$Prob[i] <- prob
}

Experiment3_CD8 <- mutate(Experiment3_CD8, Stim = ifelse(Stim == "pp1", "Pool1",
                                                         ifelse(Stim == "pp2", "Pool2",
                                                                ifelse(Stim == "pp3", "Pool3", Stim))))

ggplot(Experiment3_CD8, aes(Population, Logit_Prop, colour = Stim)) +
  geom_pointrange(aes(ymin = Logit_low, ymax = Logit_top, alpha = Prob)) +
  facet_grid(Experiment~Group)

hist(Experiment3_CD8$Prob, breaks = 100)

PP_dat_CD8 <- Experiment2_CD8 %>%
  dplyr::select(Experiment, Animal, GroupID, Stim, ParentCount)

pop_names <- names(Experiment2_CD8)[12:27]

PP_CD8 <- data_frame(Experiment = rep(PP_dat_CD8$Experiment, each = 4000),
                 Animal = rep(PP_dat_CD8$Animal, each = 4000),
                 GroupID = rep(PP_dat_CD8$GroupID, each = 4000),
                 Stim = rep(PP_dat_CD8$Stim, each = 4000),
                 ParentCount = rep(PP_dat_CD8$ParentCount, each = 4000),
                 Sim = rep(1:4000, 112))

combs_CD8 <- distinct(PP_CD8, Experiment, Animal, Stim)

for(i in 1:nrow(combs_CD8)){
  df <- PP_CD8 %>%
    filter(Experiment == combs$Experiment[i], Animal == combs$Animal[i], Stim == combs$Stim[i])
  m <- matrix(0, nrow = nrow(df), ncol = 16)
  colnames(m) <- pop_names
  for(j in 1:nrow(df)){
    if(df$Sim[j] == 1)
      print(paste(df$Animal[j], df$Stim[j]))
    if((df$Sim[j] %% 200) == 0)
      print(df$Sim[j])
    m[j, ] <- rmultinom(1, df$ParentCount[j], probs_CD8[df$Sim[j], df$GroupID[j], ])
  }
  
  df <- bind_cols(df, as_data_frame(m))
  ref <- filter(Experiment2_CD8, Animal == combs$Animal[i], Stim == combs$Stim[i])
  df <- df %>%
    gather(Population, Count, 7:22)
  ref <- ref %>%
    gather(Population, Count, 12:27)
  p <- ggplot(df, aes(Count, fill = Population)) +
    geom_histogram() +
    geom_vline(data = ref, aes(xintercept = Count), colour = "red", size = 2) +
    facet_wrap(~Population, scales = "free") +
    theme_minimal()
  pdf(paste("../Results/H-13-003/", combs$Experiment[i], "_", combs$Animal[i], "_", combs$Stim[i], "_PP_CD8.pdf", sep = ""), height = 16, width = 16)
  plot(p)
  dev.off()
}
```

```{r Export}
write_csv(Experiment3, "../Results/H-13-003/CD4 results.csv")
write_csv(Experiment3_CD8, "../Results/H-13-003/CD8 results.csv")
```
