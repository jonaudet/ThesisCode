library(checkpoint)
checkpoint("2017-07-13", scanForPackages = F)
setSnapshot("2017-07-13")
if(!require(dplyr)){
  install.packages("dplyr")
  library(dplyr)
}
if(!require(readxl)){
  install.packages("readxl")
  library(readxl)
}
if(!require(readr)){
  install.packages("readr")
  library(readr)
}
if(!require(tidyr)){
  install.packages("tidyr")
  library(tidyr)
}
if(!require(ggplot2)){
  install.packages("ggplot2")
  library(ggplot2)
}

theme_set(theme_minimal() %+replace%
            theme(axis.title = element_text(size = 14, face = "bold"),
                  axis.text = element_text(size = 10),
                  strip.text = element_text(size = 10, face = "bold"),
                  panel.spacing.x = unit(0.05, "npc"),
                  panel.spacing.y = unit(0.05, "npc"),
                  panel.grid.major = element_line(size = 0.3, colour = "grey75"),
                  panel.grid.minor = element_line(size = 0.1, colour = "grey94"),
                  legend.position = "none"))

Tcell <- read_excel("../Data/Rechallenge presample.xlsx", sheet = 1) %>% 
  right_join(read_excel("../Data/NHP_groups.xlsx")) %>% 
  group_by(Group) %>% 
  mutate(subID = as.character(as.numeric(factor(Animal)))) %>% 
  ungroup

trick <- data_frame(Animal = rep(c("A", "N", "f"), each = 8),
                    Type = rep(c("CD4", "CD8"), each = 12),
                    Assay = rep(c("Proliferation", "IFN"), each = 12),
                    Population = rep(c("Na�ve", "CM", "EM", "EMRA"), 6),
                    Pct = NA,
                    Survived = 1,
                    Group = "ZMAb_DEF201_72",
                    subID = as.character(rep(c(4, 5, 6), each = 8)))

Tcell <- bind_rows(Tcell, trick)

pdf("../Results/Rechalleng_prolif.pdf", width = 5, height = 5, useDingbats = F)
filter(Tcell, Assay == "Proliferation") %>% 
  mutate(Population = factor(Population, levels = c("Na�ve", "CM", "EM", "EMRA")),
         Survived = factor(Survived, levels = c("1", "0"))) %>% 
  ggplot(aes(Population, Pct, colour = subID, group = Animal)) +
  geom_point() +
  geom_line(aes(linetype = Survived)) +
  ylab(parse(text = "'%'~of~CFSE^low")) +
  xlab("Subset") +
  facet_grid(Type~Group)
dev.off()

expr <- paste("IFN-gamma^(''+'')~('%'~of~subset)")

pdf("../Results/Rechalleng_IFN.pdf", width = 5, height = 5, useDingbats = F)
filter(Tcell, Assay == "IFN") %>% 
  mutate(Population = factor(Population, levels = c("Na�ve", "CM", "EM", "EMRA")),
         Survived = factor(Survived, levels = c("1", "0"))) %>% 
  ggplot(aes(Population, Pct, colour = subID, group = Animal)) +
  geom_point() +
  geom_line(aes(linetype = Survived)) +
  scale_y_log10() +
  annotation_logticks(sides = "l") +
  ylab(parse(text = expr)) +
  xlab("Subset") +
  facet_grid(Type~Group)
dev.off()


surv <- distinct(Tcell, Animal, Survived)

Bcell <- read_excel("../Data/Rechallenge presample.xlsx", sheet = 2) %>% 
  gather("Animal", "Titre", 2:9) %>% 
  filter(!is.na(Titre)) %>% 
  inner_join(read_excel("../Data/NHP_groups.xlsx")) %>% 
  left_join(surv) %>% 
  group_by(Group) %>% 
  mutate(subID = as.character(as.numeric(factor(Animal)))) %>% 
  ungroup

trick <- data_frame(Animal = c("A", "N", "f"),
                    Day = 1,
                    Titre = NA,
                    Survived = 1,
                    Group = "ZMAb_DEF201_72",
                    subID = as.character(c(4, 5, 6)))

Bcell <- bind_rows(Bcell, trick)

pdf("../Results/Rechalleng_IgG.pdf", width = 5, height = 5, useDingbats = F)
mutate(Bcell,
       Survived = factor(Survived, levels = c("1", "0"))) %>% 
  ggplot(aes(Day, Titre, colour = subID, group = Animal)) +
  geom_point() +
  geom_line(aes(linetype = Survived)) +
  ylab("IgG titre [Reciprocal dilutions]") +
  scale_x_continuous("Days post-infection", breaks = seq(0, 30, 7)) +
  scale_y_log10(limits = c(1, 2048000), breaks = 10^seq(0, 6)) +
  annotation_logticks(sides = "l") +
  facet_wrap(~Group, nrow = 1)
dev.off()